var TriveniApp = angular.module('TriveniApp', []);

TriveniApp.controller('assignCtrl', function($scope) {
	$scope.selectProject = "Select Project";
	$scope.selectProgram = "Select Program";
	$scope.selectEngineer = "Select Engineer";
	$scope.manager_screen = true;
	$scope.project = false;
    $scope.program = false;
    $scope.engineer = false;
	$scope.details = false;
	$scope.engg_screen = false;
	$scope.selectedProject = function(){
		$scope.program = true;
	}
	$scope.selectedProgram = function(){
		$scope.engineer = true;
	}
	$scope.selectedEngineer = function(){
		$scope.details = true;
	}
	$scope.assign_prg = function(){
		$scope.project = false;
		$scope.program = false;
		$scope.engineer = false;
		$scope.details = false;
		$scope.engg_screen = true;
	}
	$scope.assign_new = function(){
		$scope.manager_screen = false;
		$scope.project = true;
	}
});


$(document).ready(function(){	
	$("#sign_in_show").click(function(){
		$('#sign_in_show').removeClass("hide");
		$('.login_clk').addClass("signup_active");
	});
	
	$(".login_btn").click(function(){
		var val = $("#modal-email").val();
		$(".user").text(val);
		$('#sign_in_show').addClass("hide");
		$(".login_nav").addClass("hide");
		$(".welcm_shw").removeClass("hide");
	});
	
	$(".logout").click(function(){
		$(".welcm_shw").addClass("hide");
		$(".login_nav").removeClass("hide");
		$('.cvr').removeClass("hide");
		
	});
	
	$(".assign_prgrm").click(function(){
		var cmnts = $("textarea.cmnts").val();
		var strt_dt= $("input.start_date").datepicker({ dateFormat: 'dd-M-yy' }).val();
		var cmplt_dt= $("input.cmplt_dt").datepicker({ dateFormat: 'dd-M-yy' }).val();
		$(".assignr_cmnt").text(cmnts);
		$(".strt_date").text(strt_dt);
		$(".cmpltn_date").text(cmplt_dt);
		
	});
	
	$(".submit_reload").click(function(){
		location.reload();
	});
	
});

$(document).mouseup(function(e){
	var container = $("#sign_in_show");
	var login_container = $(".login_clk");
	
	
	if (!container.is(e.target) 
		&& container.has(e.target).length === 0) 
	{
		$('#sign_in_show').addClass("hide");
		$(".login_clk").removeClass("signup_active");
	}
	$(".login_clk").click(function(){
		$('#sign_in_show').toggleClass("hide");
		$(this).toggleClass("signup_active");
	});	
	
	$(".login_btn").click(function(){
		$('.cvr').addClass("hide");
	});
	
	
});

$(function(){
	$(".selectProject li a").click(function(){
	  $(".selectProjectBtn:first-child").html($(this).text()+'<span class="arrw align_right"><span class="caret align_rt"></span></span>');
	  $(".selectedProject").text($(this).text());
	});
	
	$(".selectProgram li a").click(function(){
	  $(".selectProgramBtn:first-child").html($(this).text()+'<span class="arrw align_right"><span class="caret align_rt"></span></span>');
	  $(".selectedProgram").text($(this).text());
	});
	
	$(".selectEngineer li a").click(function(){
	  $(".selectEngineerBtn:first-child").html($(this).text()+'<span class="arrw align_right"><span class="caret align_rt"></span></span>');
	  $(".selectedEngineer").text($(this).text());
	});
});


 $(function() {
	$( "#datepicker-start" ).datepicker();
	$( "#datepicker-complt" ).datepicker();
	/* $( "#datepicker-12" ).datepicker("setDate", "10w+1"); */
});


